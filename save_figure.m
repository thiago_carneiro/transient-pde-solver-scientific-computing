%% function that saves all 112 plots as images with meaningful names.
%  Inputs:
%           ax = plot of the surface with the temperature distribution 
%           Nx = grid resolution in x direction
%           Ny = grid resolution in y direction
%           dt = time step
%           s = index of the respective time step in the 'dts' vector
%           method = method = "explicit euler" or "implicit euler"
%

function save_figure(ax,Nx,Ny,dt,s,method)

hfig = figure('visible','off','WindowState','maximized');
ax_new = copyobj(ax, hfig);
set(ax_new, 'Position', get(0, 'DefaultAxesPosition'));
% saveas(hfig,method+"_t="+num2str(dt*s)+"_Nx="+num2str(Nx)+"_Ny="+num2str(Ny)+"_dt="+num2str(dt)+".png",'png');

imwrite(getframe(hfig).cdata, method+"_t="+num2str(dt*s)+"_Nx="+num2str(Nx)+"_Ny="+num2str(Ny)+"_dt="+num2str(dt)+".png");

% print(hfig,method+"_t="+num2str(dt*s)+"_Nx="+num2str(Nx)+"_Ny="+num2str(Ny)+"_dt="+num2str(dt)+".jpeg",'-djpeg');

% exportgraphics(ax,method+"_t="+num2str(dt*s)+"_Nx="+num2str(Nx)+"_Ny="+num2str(Ny)+"_dt="+num2str(dt)+".png");
end