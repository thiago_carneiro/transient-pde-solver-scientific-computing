%% Function that solves the heat equation depending on the method (explicit euler or implicit euler)
%   Inputs:
%           Nx = resolution of the grid in x direction
%           Ny = resolution of the grid in y direction
%           dt = time step 
%           T_initial = grid with initial Dirichlet BC
%           method = "explicit euler" or "implicit euler"
%

function T = solve_heat_equation(Nx,Ny,dt,T_initial,method)

if method == "explicit euler"
    T = explicit_euler_step(Nx,Ny,dt,T_initial); % solves the heat equation by explicit euler scheme
elseif method == "implicit euler"
    T = implicit_euler_step(Nx,Ny,dt,T_initial); % solves the heat equation by implicit euler scheme
end
end