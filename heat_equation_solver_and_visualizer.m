%% Function that solves the instationary heat equation and generates the graphs
%  Inputs:  
%               N = Nx = Ny = grid resolution in the x and y directions;
%               dts = vector with definition of the time steps
%               capture_ts = vector with definition of the times
%               capture_ts_labels = string array with definition of the times 
%               initial_f = initial values of Dirichlet BC for the inner
%                           points of the grid
%               method = "explicit euler" or "implicit euler"
%               rows = size of the vector N
%               cols = size of the vector dts
%

function heat_equation_solver_and_visualizer(N,dts,capture_ts,capture_ts_labels,initial_f, method,rows,cols)

N_length = size(N,1);
dts_length = size(dts,1); % size of the vector with time step values
capture_ts_length = size(capture_ts,1); % size of the vector with time values
last_capture_t = max(capture_ts); % get maximum time value
figs = cell(capture_ts_length,1); % cell array with the values of the time  
for f=1:capture_ts_length
    figs{f} = figure('Name',"method="+method+" t="+capture_ts_labels(f),'Visible','off'); % generate the figures with the respective method and time
end
fig_index = 1;
for n=1:N_length
    Nx = N(n,1);
    Ny = N(n,2);
    T_initial = generate_T(Nx,Ny,initial_f); % generate grid of size (Nx x Ny)
    for k=1:dts_length
        dt = dts(k);
        steps = floor(last_capture_t/dt);
        T = T_initial;
        for s=1:steps
            T = solve_heat_equation(Nx,Ny,dt,T,method); % function that solves the heat eq. depending on the method (explicit or implicit euler)
            if any(dt*s == capture_ts(:))
                set(0, 'CurrentFigure', figs{fig_index});
                ax = surface_plot(T,Nx,Ny,(n-1)*dts_length+k,rows,cols); % plot surface with temperature distribution
                [nom,denom] = rat(dt);
                title([method+" t="+capture_ts_labels(fig_index),"Nx="+num2str(Nx)+",Ny="+num2str(Ny)+",dt="+nom+"/"+denom]);
                if method == "explicit euler"
                    save_figure(ax,Nx,Ny,dt,s,method); % function that saves all 112 plots as images 
                                                        % with meaningful names.
                end
                fig_index = mod(fig_index,capture_ts_length) + 1;
            end
        end
    end
end
for f=1:capture_ts_length
    set(figs{f},{'Visible','WindowState'},{'on','maximized'});
end
end