%% function that performs the explicit Euler scheme
%  Inputs: 
%             Nx =  x resolution
%             Ny = y resolution 
%             dt  =  time step
%             Tn = 2D vector representing the mesh grid
%
function T = explicit_euler_step(Nx, Ny, dt, Tn)

kx = dt * (Nx+1)^2; % initialize the term kx = dt * (1/hx^2)
ky = dt * (Ny+1)^2; % initialize the term ky = dt * (1/hy^2)

k = 1 + dt * (-2*(Nx+1)^2  - 2*(Ny+1)^2);
T = zeros(Ny,Nx);
Tn_grid = zeros(Ny+2,Nx+2);
Tn_grid(2:Ny+1,2:Nx+1) = reshape(Tn,Nx,Ny)';
for j=2:Ny+1
    for i=2:Nx+1
        T(j-1,i-1) = k*Tn_grid(j,i) + kx*(Tn_grid(j,i-1)+Tn_grid(j,i+1)) ... % loop that performs explicit Euler 
                                    + ky*(Tn_grid(j-1,i)+Tn_grid(j+1,i));
    end
end
T = reshape(T',Nx*Ny,1); % reshape of the grid
end