%% Function that performs the Gauss-Seidel method
%  Inputs:
%               b = b_vector (from Ax=b)
%               Nx = resolution of the grid in x direction
%               Ny = resolution of the grid in y direction
%               dt = time step


function grid = gauss_seidel(b,Nx,Ny,dt)

grid = zeros(Ny+2,Nx+2);
hx_2_inv = (Nx+1)*(Nx+1);
hy_2_inv = (Ny+1)*(Ny+1);
kx = -dt * hx_2_inv;
ky = -dt * hy_2_inv;
k = 1/(1 + 2*dt*hx_2_inv + 2*dt*hy_2_inv);
residual_norm = inf;

while residual_norm > 10^-6 % residual of to stop the loop of Gauss-Seidel iteration

    b_index = 1;
    for j=2:Ny+1
        for i=2:Nx+1
            grid(j,i) = (b(b_index) - (grid(j,i-1)+grid(j,i+1))*kx - ... % equation derived from the Stencil method
                (grid(j-1,i)+grid(j+1,i))*ky)*k; 

            b_index = b_index + 1;
        end
    end

    residual_sub_sum = 0;
    b_index = 1;
    for j=2:Ny+1
        for i=2:Nx+1
            residual_sub_sum = residual_sub_sum + ... % sum that enters in the calculation of the residual
                (b(b_index)-grid(j,i)*(1/k)- ...
                (grid(j,i-1)+grid(j,i+1))*kx- ...
                (grid(j-1,i)+grid(j+1,i))*ky)^2;
            b_index = b_index + 1;
        end
    end
    residual_norm = sqrt((1/(Nx*Ny))*residual_sub_sum); % calculation of the residual 
end
grid = reshape(grid(2:Ny+1,2:Nx+1)', Nx*Ny, 1); % reshape of the grid
end