%% Function that solves the heat equation by the implicit Euler scheme 
%  The function calls the gauss_seidel function to apply the Gauss-Seidel
%  method to solve the heat equation 
%
% Inputs:
%           Nx = resolution of the grid in x direction 
%           Ny = resolution of the grid in y direction
%           dt = time step
%           Tn = grid with initial Dirichlet BC

function T = implicit_euler_step(Nx, Ny, dt, Tn)
T = gauss_seidel(Tn,Nx,Ny,dt); % calls the Gauss - Seidel method
end