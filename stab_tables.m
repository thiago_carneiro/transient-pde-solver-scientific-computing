%% Function that generates a boolean tables that show when the explicit euler method is stable (true) or unstable (false)
% Inputs:
%        N = vector with the grid resolution values 
%        dts = vector with the time step values
%

function stab_tables(N,dts)
    stab=false(length(N),length(dts));
    for i=1:length(dts)
        %Time step has to be smaller than (h^2)/2
        stab(:,i)=dts(i)<((1+N(:,1)).^-2)/2;
    end
    disp(table(N(:,1),stab(:,1),stab(:,2),stab(:,3),stab(:,4),stab(:,5),stab(:,6),stab(:,7), ...
        'VariableNames',{'Nx=Ny','dt=1/64','dt=1/128','dt=1/256','dt=1/512','dt=1/1024','dt=1/2048','dt=1/4096'}));
end