%% Function that creates the mesh grid with the initial Diriclet BC
%       T(x, y, 0) = 1.0 forall (x, y) in ]0; 1[^2
% Inputs:
%            Nx = resolution of the grid in x direction 
%            Ny = resolution of the grid in y direction 
%            f = function with homogeneous Dirichlet BC values

function T = generate_T(Nx,Ny,f)

T = zeros(Ny,Nx);
hx = 1/(Nx+1);
hy = 1/(Ny+1);

for j=1:Ny
    for i=1:Nx
        T(j,i) = f(i*hx,j*hy); % set of the points of the grid with the Dirichlet BC values
    end
end
T = reshape(T',Nx*Ny,1);
end