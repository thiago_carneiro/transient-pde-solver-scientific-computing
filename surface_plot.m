%% Function that performs the plot of the temperature distribution over a surface
%  Inputs:
%          T = grid with the temperature values distribution 
%          Nx = resolution of the grid in the x direction  
%          Ny = resolution of the grid in the y direction 
%          i = position of the subplot
%          rows = size of the vector N
%          cols = size of the vector dts

function ax = surface_plot(T,Nx,Ny,i,rows,cols)
    hx=1/(Nx+1);
    hy=1/(Ny+1);
    [X,Y]=meshgrid(0:hx:1,0:hy:1);
    Z=zeros(Ny+2,Nx+2);
    Z(2:Ny+1,2:Nx+1) = reshape(T,Nx,Ny)';
    ax = subplot(rows,cols,i);
    surf(X,Y,Z,'FaceColor','interp');
    xlabel('x');
    ylabel('y');
    zlabel('T');
end