clear all;
clc;

N = [3,3;7,7;15,15;31,31]; % definition of Nx and Ny
dts = [1/64;1/128;1/256;1/512;1/1024;1/2048;1/4096]; % definition of the time steps
capture_ts = [1/8;2/8;3/8;4/8]; % definition of the times 
capture_ts_labels = ["1/8";"2/8";"3/8";"4/8"];

stab_tables(N,dts); % generate table with boolean values for when the explicit euler method is stable or unstable

initial_f = @(x,y) 1; % function to set Dirichlet BC at initial time t = 0

heat_equation_solver_and_visualizer(N,dts,capture_ts, ... % function that solves the instationary heat equation
         capture_ts_labels, initial_f, "explicit euler", size(N,1),size(dts,1)); % and generates the graphs 

dts_implicit = [1/64];

heat_equation_solver_and_visualizer(N,dts_implicit, ... % function that solves the instationary heat equation
    capture_ts, capture_ts_labels, initial_f, "implicit euler",2,2);